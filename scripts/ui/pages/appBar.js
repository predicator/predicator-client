var fastn = require('^fastn'),
    app = require('^app');


module.exports = function(){
    return fastn('div', {class: 'appBar'},
        fastn('div',{class:'menu'},fastn('i', {'class': 'mdi mdi-menu'}) )
            .on('click', function(event, scope){
                app.pages.setPage('documents')
            }),
        fastn('div',{class:'title'}, 'PREDICATOR' ),
        fastn('div',{class:'menu'},fastn('i', {'class': 'mdi mdi-dots-vertical'}) )
            .on('click', function(){
                console.log('setting click');  
            })
        
    )
};