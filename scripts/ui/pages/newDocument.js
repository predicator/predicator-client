var fastn = require('^fastn'),
    app = require('^app');

module.exports = function(){
    return fastn('div',
        fastn('textarea', 
            {
                class:'predicateTitle',
                disabled: fastn.binding('disabled').attach(app.disable.model),
                // value: fastn.binding('document.title').attach(app.documents.documentModel),
                value: app.documents.title,
                placeholder: 'Predicate title',
                onkeyup: 'value:value'
            }
        ),
        fastn('div',
          fastn('textarea', 
            { 
                class: 'predicateInput',
                disabled: fastn.binding('disabled').attach(app.disable.model),
                rows: 40,
                value: app.documents.predicate, 
                // value: fastn.binding('document.predicate').attach(app.documents.documentModel),
                placeholder: 'Type your predicate text here',
                onkeyup: 'value:value'
            })
        ),
        require('./disable')('create')
    );
};
