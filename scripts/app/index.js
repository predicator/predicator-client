module.exports = {
    documents: require('./services/documents'),
    pages: require('./services/pages'),
    session: require('./services/session'),
    disable: require('./services/disable')
};