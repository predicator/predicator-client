
assets: styles scripts

# build single js file starting from index.js
scripts: $(shell find scripts -type f) 
  watchify scripts/index.js -o 'uglifyjs -cm > index.browser.js' -v

# build css resource from stylus assets
styles: $(shell find built/styles -type f)
  stylus --include-css built/styles/index.styl -o built/styles/

# this is a helper target during developmnt to rebuild any assets
# nodemon just runs a command when any of the files change
# we run `make assets` when js, styl, or html files change
# in the js or css folders

devwatch: 
  $(nodemon) -w scripts -w styles -e js,styl --exec "make assets" 
  #supervisor -w scripts,styles -e js,styl -x make assets