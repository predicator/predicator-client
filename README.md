# Predicator Client

Basic client app using GFC to accompany Predicator Server

## Goals

The goals are to demonstrate an app which uses GFC in the back-end

## Requirements

The app client will 

* Demonstrate an example of a View
* Demonstrate local Storage<->gfc synchronisation
* Display a login page
* Display a home page
* Display menu options to navigate to

1. A list of documents 
2. A display of document data
3. A form allow updating of data
4. A new form allowing entry of data 


properties:{type:document}
5630b4e52f8de60265e0ad6b

type:document,
properties:{
"i":"5630b3142f8de60265e0ad6a",
  